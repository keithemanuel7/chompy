﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chompy.Services
{
    // Right click the project, hit manage user secrets, and set these values in the secrets.json file.
    public class AuthMessageSenderOptions
    {
        public string AuthMessageUsername { get; set; }
        public string AuthMessagePassword { get; set; }
    }
}
