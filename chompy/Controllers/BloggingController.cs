﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Chompy.Models;
using Chompy.Models.BloggingViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace Chompy.Controllers
{
    public class BloggingController : Controller
    {
        private BloggingContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public BloggingController(
            BloggingContext context,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Posts()
        {
            ViewData["Message"] = "Your posts page.";

            List<Post> posts = _context
                    .Posts
                    .Include(p => p.PostTags)
                        .ThenInclude(pt => pt.Tag)
                    .Where(p => p.Published)
                    .ToList();

            List<Tag> tags = _context
                    .Tags
                    .ToList();

            PostsViewModel postsViewModel = new PostsViewModel() {
                PostsJson = JsonConvert.SerializeObject(posts, Formatting.Indented, new JsonSerializerSettings(){ ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                TagsJson = JsonConvert.SerializeObject(tags),
            };

            return View(postsViewModel);
        }

        public IActionResult Post(string id)
        {
            PostViewModel postViewModel = null;

            try
            {
                postViewModel = new PostViewModel()
                {
                    Post = _context.Posts
                    .Where(p => p.Published)
                    .Single(p => p.UrlPath == id)
                };
            }
            catch (Exception ex)
            {
                // todo: Log
            }

            if(postViewModel == null)
            {
                return RedirectToAction("Posts");
            }

            postViewModel.MorePosts = _context
                .Posts
                .Where(p => p.PostId != postViewModel.Post.PostId && p.Published)
                .OrderByDescending(p => p.ModifiedTimestamp)
                .ToList();

            return View(postViewModel);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Editor(string id = null)
        {
            EditorViewModel editorViewModel;

            // If we have the url path to a post to edit
            if(id != null)
            {
                Post post = _context.Posts
                    .Include(p => p.PostTags)
                        .ThenInclude(pt => pt.Tag)
                    .Single(p => p.UrlPath == id);

                editorViewModel = new EditorViewModel()
                {
                    PostId = post.PostId,
                    Title = post.Title,
                    UrlPath = post.UrlPath,
                    TagString = string.Join(" ", post.PostTags.Select(pt => pt.Tag.Label)),
                    Blurb = post.Blurb,
                    Content = post.Content,
                };
            }
            else // If we have are create a new post
            {
                editorViewModel = new EditorViewModel()
                {
                    Title = "Enter your title",
                    UrlPath = "edit-the-url-here",
                    TagString = "enter-tags-here seperate-with-spaces",
                    Blurb = "Enter your blurb here",
                    Content = "<h2>Use this area to edit your post.</h2>",
                };
            }

            string userEmail = _userManager.GetUserAsync(this.User).Result.Email;

            editorViewModel.BlogOptions = _context.Blogs
                .Include(b => b.Publishers)
                    .ThenInclude(p => p.Person)
                .Where(b => b.Publishers.Select(p => p.Person.EmailAddress).Contains(userEmail))
                .Select(b => new SelectListItem() { Text = b.Name, Value = b.BlogId.ToString() })
                .ToList();

            // Order the posts for the sidebar with drafts first, then by descending mod timestamps.
            editorViewModel.Posts = _context
                .Posts
                .OrderBy(p => p.Published)
                    .ThenByDescending(p => p.ModifiedTimestamp)
                .ToList();
            
            return View(editorViewModel);
        }

        /// <summary>
        /// todo: [Bind(Include: "")]?
        /// </summary>
        /// <param name="editorViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        [Route("[controller]/Editor/SaveDraftPost")]
        public IActionResult SaveDraftPost(EditorViewModel editorViewModel)
        {
            if (ModelState.IsValid)
            {
                editorViewModel.Publish = false;
                SaveEditorPost(editorViewModel);
            }

            return RedirectToAction("Editor", new { id = editorViewModel.UrlPath });
        }

        /// <summary>
        /// todo
        /// </summary>
        /// <param name="editorViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        [Route("[controller]/Editor/PublishPost")]
        public IActionResult PublishPost(EditorViewModel editorViewModel)
        {
            if (ModelState.IsValid)
            {
                editorViewModel.Publish = true;
                SaveEditorPost(editorViewModel);
            }

            return RedirectToAction("Editor", new { id = editorViewModel.UrlPath });
        }

        /// <summary>
        /// todo
        /// </summary>
        /// <param name="editorViewModel"></param>
        private void SaveEditorPost(EditorViewModel editorViewModel)
        {
            // create/update the Post
            Post post;

            if (editorViewModel.PostId.HasValue)
            {
                post = _context.Posts.Single(p => p.PostId == editorViewModel.PostId);

                post.Title = editorViewModel.Title;
                post.UrlPath = editorViewModel.UrlPath;
                post.Blurb = editorViewModel.Blurb;
                post.Content = editorViewModel.Content;
                post.Published = editorViewModel.Publish;

                _context.Posts.Update(post);
            }
            else
            {
                post = new Post()
                {
                    BlogId = editorViewModel.SelectedBlogId,
                    Title = editorViewModel.Title,
                    UrlPath = editorViewModel.UrlPath,
                    Blurb = editorViewModel.Blurb,
                    Content = editorViewModel.Content,
                    Published = editorViewModel.Publish,
                };

                _context.Posts.Add(post);
            }

            _context.SaveChanges();

            List<string> tagStrings = editorViewModel.TagString.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            // Insert new Tags
            Tag.InsertIfNotExists(_context, tagStrings);
            _context.SaveChanges();

            // Get the new Tags
            List<Tag> tags = _context
                .Tags
                .Where(t => tagStrings.Contains(t.Label))
                .ToList();

            List<PostTag> postTagsToInsert = tags.Select(t => new PostTag()
                {
                    PostId = post.PostId,
                    TagId = t.TagId,
                })
                .Where(pt1 => !_context.PostTags.Any(pt2 => pt1.PostId == pt2.PostId && pt1.TagId == pt2.TagId))
                .ToList();

            // Add new PostTags
            _context.PostTags
                .AddRange(postTagsToInsert);

            // Remove old PostTags
            _context.PostTags.RemoveRange(
                _context.PostTags.Where(pt => pt.PostId == post.PostId && !tags.Contains(pt.Tag)));

            _context.SaveChanges();
        }
    }
}
