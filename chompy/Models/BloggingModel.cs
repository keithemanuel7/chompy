﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading;

/// <summary>
/// todo
/// </summary>
namespace Chompy.Models
{


    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Subscriber> Subscribers { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<PostTag> PostTags { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Person> People { get; set; }

        public BloggingContext(DbContextOptions<BloggingContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // todo: change to data annotation when possible (not in ef core yet)
            // not sure if this constraint is being created
            modelBuilder.Entity<Post>()
                .HasIndex(p => new { p.UrlPath, p.BlogId })
                .IsUnique();

            modelBuilder.Entity<Person>()
                .HasIndex(p => p.EmailAddress)
                .IsUnique();
        }

        public override int SaveChanges()
        {
            OnSaveBloggingBaseEnitity();
            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnSaveBloggingBaseEnitity();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            OnSaveBloggingBaseEnitity();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            OnSaveBloggingBaseEnitity();
            return base.SaveChangesAsync(cancellationToken);
        }

        /// <summary>
        /// This function is used to automattically update metadata columns (CreatedTimestamp, ModifiedTimestamp, etc...) on Blogging Tables
        /// </summary>
        private void OnSaveBloggingBaseEnitity()
        {
            var entities = ChangeTracker.Entries()
                .Where(x => x.Entity is BloggingBaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach(var entity in entities)
            {
                if(entity.State == EntityState.Added)
                {
                    ((BloggingBaseEntity)entity.Entity).CreatedTimestamp = DateTime.UtcNow;
                }
                
                ((BloggingBaseEntity)entity.Entity).ModifiedTimestamp = DateTime.UtcNow;
            }
        }
    }

    /// <summary>
    /// todo
    /// </summary>
    [Table("Blog")]
    public class Blog : BloggingBaseEntity
    {
        public int BlogId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required][MaxLength(2000)]
        public string Url { get; set; }

        public List<Publisher> Publishers { get; set; }

        public List<Post> Posts { get; set; }
        public List<Subscriber> Subscribers { get; set; }
    }

    [Table("Publisher")]
    public class Publisher : BloggingBaseEntity
    {
        public int PublisherId { get; set; }

        public int BlogId { get; set; }
        public Blog Blog { get; set; }

        public int PersonId { get; set; }
        public Person Person { get; set; }

        public bool IsAdmin { get; set; }
    }

    [Table("Subscriber")]
    public class Subscriber : BloggingBaseEntity
    {
        public int SubscriberId { get; set; }
        public int BlogID { get; set; }
        public Blog Blog { get; set; }
        [Required][EmailAddress]
        public string EmailAddress { get; set; }
        public bool Active { get; set; }
    }

    /// <summary>
    /// todo
    /// </summary>
    [Table("Post")]
    public class Post : BloggingBaseEntity
    {
        public int PostId { get; set; }
        public int BlogId { get; set; }
        public Blog Blog { get; set; }
        [Required] [MaxLength(128)]
        public string Title { get; set; }
        [Required] [RegularExpression("^[a-zA-Z][a-zA-Z0-9'-']*[a-zA-Z]$")] [Display(Name = "Url Path")][MaxLength(64)]
        public string UrlPath { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public string Blurb { get; set; }
        public bool Published { get; set; }

        /// <summary>
        /// Used for when the submitted form inserts new tags from the ViewModel.
        /// </summary>
        [NotMapped]
        public int TagsString { get; set; }

        public List<PostTag> PostTags { get; set; }
    }

    /// <summary>
    /// todo
    /// </summary>
    [Table("PostTag")]
    public class PostTag : BloggingBaseEntity
    {
        public int PostTagId { get; set; }

        public int PostId { get; set; }
        public Post Post { get; set; }

        public int TagId { get; set; }
        public Tag Tag { get; set; }
    }

    /// <summary>
    /// todo
    /// </summary>
    [Table("Tag")]
    public class Tag : BloggingBaseEntity
    {
        public int TagId { get; set; }
        [Required][MaxLength(64)][RegularExpression("^[a-zA-Z][a-zA-Z0-9'-']*[a-zA-Z]$")]
        public string Label { get; set; }

        List<PostTag> PostTags { get; set; }


        /// <summary>
        /// Inserts new tags into the database. 
        /// </summary>
        /// <param name="insertTags">The list of tags too add, if the do not already exist.</param>
        /// <returns>The list of tags that were inserted.</returns>
        public static List<string> InsertIfNotExists(BloggingContext context, List<string> insertTags)
        {
            // Create a list of only new tags
            List<string> newTags = insertTags.Except(context.Tags.Select(t => t.Label)).ToList();
            // Add the new tags to the database
            context.Tags.AddRange(newTags.Select(t => new Tag() { Label = t}).ToList());
            // Return the list of new tags
            return newTags;
        }
    }

    /// <summary>
    /// todo
    /// </summary>
    [Table("Person")]
    public class Person : BloggingBaseEntity
    {
        public int PersonId { get; set; }
        [Required][MaxLength(64)]
        public string GivenName { get; set; }
        [Required][MaxLength(64)]
        public string Surname { get; set; }
        [MaxLength(64)]
        public string DisplayName { get; set; }
        [Required][EmailAddress]
        public string EmailAddress { get; set; }
        [MaxLength(254)]
        public string PhoneNumber { get; set; }
        // todo: add social media
    }
}
