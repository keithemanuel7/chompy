﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chompy.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new BloggingContext(serviceProvider.GetRequiredService<DbContextOptions<BloggingContext>>()))
            {
                if (context.Blogs.Any())
                {
                    return;
                }

                context.Blogs.Add(
                    new Blog()
                    {
                        Name = "Keith Emanuel",
                        Url = "keithemanuel.com"
                    });

                context.SaveChanges();

                context.People.Add(
                    new Person()
                    {
                        GivenName = "Keith",
                        Surname = "Emanuel",
                        EmailAddress = "keithemanuel7@gmail.com"
                    });

                context.SaveChanges();

                context.Publishers.Add(
                    new Publisher()
                    {
                        BlogId = context.Blogs.First().BlogId,
                        PersonId = context.People.First().PersonId,
                        IsAdmin = true
                    });

                context.SaveChanges();
            }
        }
    }
}
