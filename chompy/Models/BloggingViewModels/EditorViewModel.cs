﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Chompy.Models.BloggingViewModels
{
    public class EditorViewModel
    {
        public List<SelectListItem> BlogOptions { get; set; }

        [Required]
        [Display(Name = "Blog")]
        public int SelectedBlogId { get; set; }

        public int? PostId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Url Path")]
        public string UrlPath { get; set; }

        [Required]
        [Display(Name = "Tags")]
        public string TagString { get; set; }

        [Required]
        public string Blurb { get; set; }

        public string Content { get; set; }

        public bool Publish { get; set; }

        public List<Post> Posts { get; set; }
    }
}
