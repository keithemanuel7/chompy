﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Chompy.Models.BloggingViewModels
{
    public class PostsViewModel
    {
        [Required]
        public string PostsJson { get; set; }
       
        [Required]
        public string TagsJson { get; set; }
    }
}
