﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Chompy.Models.BloggingViewModels
{
    public class PostViewModel
    {
        [Required]
        public Post Post { get; set; }

        public List<Post> MorePosts { get; set; }
    }
}
