﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Chompy.Models;

namespace Chompy.Migrations
{
    [DbContext(typeof(BloggingContext))]
    partial class BloggingContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Chompy.Models.Blog", b =>
                {
                    b.Property<int>("BlogId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedTimestamp");

                    b.Property<DateTime>("ModifiedTimestamp");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Url")
                        .IsRequired()
                        .HasMaxLength(2000);

                    b.HasKey("BlogId");

                    b.ToTable("Blog");
                });

            modelBuilder.Entity("Chompy.Models.Person", b =>
                {
                    b.Property<int>("PersonId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedTimestamp");

                    b.Property<string>("DisplayName")
                        .HasMaxLength(64);

                    b.Property<string>("EmailAddress")
                        .IsRequired();

                    b.Property<string>("GivenName")
                        .IsRequired()
                        .HasMaxLength(64);

                    b.Property<DateTime>("ModifiedTimestamp");

                    b.Property<string>("PhoneNumber")
                        .HasMaxLength(254);

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasMaxLength(64);

                    b.HasKey("PersonId");

                    b.HasIndex("EmailAddress")
                        .IsUnique();

                    b.ToTable("Person");
                });

            modelBuilder.Entity("Chompy.Models.Post", b =>
                {
                    b.Property<int>("PostId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BlogId");

                    b.Property<string>("Blurb")
                        .IsRequired();

                    b.Property<string>("Content")
                        .IsRequired();

                    b.Property<DateTime>("CreatedTimestamp");

                    b.Property<DateTime>("ModifiedTimestamp");

                    b.Property<bool>("Published");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("UrlPath")
                        .IsRequired()
                        .HasMaxLength(64);

                    b.HasKey("PostId");

                    b.HasIndex("BlogId");

                    b.HasIndex("UrlPath", "BlogId")
                        .IsUnique();

                    b.ToTable("Post");
                });

            modelBuilder.Entity("Chompy.Models.PostTag", b =>
                {
                    b.Property<int>("PostTagId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedTimestamp");

                    b.Property<DateTime>("ModifiedTimestamp");

                    b.Property<int>("PostId");

                    b.Property<int>("TagId");

                    b.HasKey("PostTagId");

                    b.HasIndex("PostId");

                    b.HasIndex("TagId");

                    b.ToTable("PostTag");
                });

            modelBuilder.Entity("Chompy.Models.Publisher", b =>
                {
                    b.Property<int>("PublisherId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BlogId");

                    b.Property<DateTime>("CreatedTimestamp");

                    b.Property<bool>("IsAdmin");

                    b.Property<DateTime>("ModifiedTimestamp");

                    b.Property<int>("PersonId");

                    b.HasKey("PublisherId");

                    b.HasIndex("BlogId");

                    b.HasIndex("PersonId");

                    b.ToTable("Publisher");
                });

            modelBuilder.Entity("Chompy.Models.Subscriber", b =>
                {
                    b.Property<int>("SubscriberId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active");

                    b.Property<int>("BlogID");

                    b.Property<DateTime>("CreatedTimestamp");

                    b.Property<string>("EmailAddress")
                        .IsRequired();

                    b.Property<DateTime>("ModifiedTimestamp");

                    b.HasKey("SubscriberId");

                    b.HasIndex("BlogID");

                    b.ToTable("Subscriber");
                });

            modelBuilder.Entity("Chompy.Models.Tag", b =>
                {
                    b.Property<int>("TagId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedTimestamp");

                    b.Property<string>("Label")
                        .IsRequired()
                        .HasMaxLength(64);

                    b.Property<DateTime>("ModifiedTimestamp");

                    b.HasKey("TagId");

                    b.ToTable("Tag");
                });

            modelBuilder.Entity("Chompy.Models.Post", b =>
                {
                    b.HasOne("Chompy.Models.Blog", "Blog")
                        .WithMany("Posts")
                        .HasForeignKey("BlogId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Chompy.Models.PostTag", b =>
                {
                    b.HasOne("Chompy.Models.Post", "Post")
                        .WithMany("PostTags")
                        .HasForeignKey("PostId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Chompy.Models.Tag", "Tag")
                        .WithMany()
                        .HasForeignKey("TagId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Chompy.Models.Publisher", b =>
                {
                    b.HasOne("Chompy.Models.Blog", "Blog")
                        .WithMany("Publishers")
                        .HasForeignKey("BlogId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Chompy.Models.Person", "Person")
                        .WithMany()
                        .HasForeignKey("PersonId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Chompy.Models.Subscriber", b =>
                {
                    b.HasOne("Chompy.Models.Blog", "Blog")
                        .WithMany("Subscribers")
                        .HasForeignKey("BlogID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
